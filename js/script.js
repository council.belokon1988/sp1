$(document).ready(function(){
	var items = $('#stage li'),
		itemsByTags = {};
	items.each(function(i){
		var elem = $(this),
			tags = elem.data('tags').split(',');
		elem.attr('data-id',i);
		$.each(tags,function(key,value){
			value = $.trim(value);
			if(!(value in itemsByTags)){
				itemsByTags[value] = [];
			}
			itemsByTags[value].push(elem);
		});
	});

	createList('All',items);

	$.each(itemsByTags,function(k,v){
		createList(k,v);
	});
	$('body').on('click', '#filter a', function(e){
		var link = $(this);
		link.addClass('active').siblings().removeClass('active');
		$('#stage').quicksand(link.data('list').find('li'));
		e.preventDefault();
	});
	$('#filter a:first').click();
	
	function createList(text,items){
		var ul = $('<ul>',{'class':'hidden'});
		$.each(items,function(){
			$(this).clone().appendTo(ul);
		});
		ul.appendTo('#container');
		var a = $('<a>',{
			html: text,
			href:'#',
			data: {list:ul}
		}).appendTo('#filter');
	}
	
	$('.team-slider').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		asNavFor: '.team-slider-nav',
		autoplay: false,
		autoplaySpeed: 2000
	});
	$('.team-slider-nav').slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		asNavFor: '.team-slider',
		focusOnSelect: true,
		variableWidth: true,
		prevArrow: '<span class="team-slide-nav prev"><img src="images/icon/arrow-l.png"/></span>',
		nextArrow: '<span class="team-slide-nav next"><img src="images/icon/arrow-r.png"/></span>'
	});	
	
	$('body').on('click', '.preload-container button', function(e){
		$('.preload-container').addClass('hidden');
		$('.stage-hide').removeClass('stage-hide');
	});	
	
});